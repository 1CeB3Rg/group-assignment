from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import BuyForm, Coupon, Review1M, Review2M, Review3M
from .forms import BuyerForm, CouponForm, Review1,Review2,Review3

def home(request):
    return render(request,'home.html')

def buyerformview(request):
    if request.method == 'POST':
        form = BuyerForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
        return redirect('/table/')
    else:
        form = BuyerForm()
        return render (request, 'buyerform.html', {'form' : form})

def coupon(request):
    return render(request,"coupon.html")

def table(request):
    data = BuyForm.objects.all()
    context = {'Data': data}
    return render( request, "table.html", context)
def itemlist(request):
	return render(request, "item-list.html")

def item1(request):
    if request.method == 'POST':
        form = Review1(request.POST)
        if form.is_valid():
            data_item= form.save(commit=False)
            data_item.save()
        return redirect('/item1')
    form= Review1()
    data= Review1M.objects.all()
    response={'Data':data,'status':form,}
    return render(request,"item1.html",response)
def item2(request):
    if request.method == 'POST':
        form = Review2(request.POST)
        if form.is_valid():
            data_item= form.save(commit=False)
            data_item.save()
        return redirect('/item2')
    forms= Review2()
    datas= Review2M.objects.all()
    response={'Datas':datas,'statuss':forms,}
    return render(request,"item2.html",response)
def item3(request):
    if request.method == 'POST':
        form = Review3(request.POST)
        if form.is_valid():
            data_item= form.save(commit=False)
            data_item.save()
        return redirect('/item3')
    formss= Review3()
    datass= Review3M.objects.all()
    response={'Datass':datass,'statusss':formss,}
    return render(request,"item3.html",response)
