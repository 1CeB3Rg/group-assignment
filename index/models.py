from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class BuyForm(models.Model):
    item_list = [
    ('item1', "Iphone X Improved"),
    ('item2', "Samsung Galaxy A80 - With Headphone Jack"),
    ('item3', "MacBook Air Improved"),
    ]
    item_count = [
    ('1', "1"),
    ('2', "2"),
    ('3', "3"),
    ]
    coupon_list = [
    ('0', "None"),
    ('X', "Voucher-X"),
    ('Y', "Voucher-Y"),
    ('Z', "Voucher-Z"),
    ('L', "Voucher-L"),

    ]
    name = models.CharField(max_length=50)
    items = models.CharField(
        max_length=15,
        choices=item_list,
    )
    total = models.CharField(
        max_length=2,
        choices=item_count,
        default=1
    )
    coupon = models.CharField(
        max_length=10,
        choices=coupon_list,
        default="None",
        )
    date = models.DateTimeField(default=datetime.now, editable=False)
    email = models.CharField(max_length=20, default="youremail")
    phone = models.IntegerField(default="0")
    address = models.TextField(default="None")
    def __str__(self):
        return self.name

class Coupon(models.Model):
    discount_list = [
    ("20", "20% Discount"),
    ("30", "30% Discount"),
    ]
    code = models.CharField(max_length=10)
    discount = models.CharField(
        max_length=2,
        choices=discount_list,
    )
class Review1M(models.Model):
    item_count = [
    ('1', "1"),
    ('2', "2"),
    ('3', "3"),
    ('4', "4"),
    ('5', "5"),
    ]
    Name= models.CharField(max_length=30)
    Star = models.CharField(
        max_length=2,
        choices=item_count,
        default=1
    )
    Review=models.CharField(max_length=500) 
    def __str__(self):
        return self.Name
class Review2M(models.Model):
    item_count = [
    ('1', "1"),
    ('2', "2"),
    ('3', "3"),
    ('4', "4"),
    ('5', "5"),
    ]
    Name= models.CharField(max_length=30)
    Star = models.CharField(
        max_length=2,
        choices=item_count,
        default=1
    )
    Review=models.CharField(max_length=500) 
    def __str__(self):
        return self.Name
class Review3M(models.Model):
    item_count = [
    ('1', "1"),
    ('2', "2"),
    ('3', "3"),
    ('4', "4"),
    ('5', "5"),
    ]
    Name= models.CharField(max_length=30)
    Star = models.CharField(
        max_length=2,
        choices=item_count,
        default=1
    )
    Review=models.CharField(max_length=500) 
    def __str__(self):
        return self.Name