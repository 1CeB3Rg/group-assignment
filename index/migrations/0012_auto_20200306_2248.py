# Generated by Django 3.0.3 on 2020-03-06 15:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0011_auto_20200306_2226'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review2m',
            old_name='Names',
            new_name='Name',
        ),
        migrations.RenameField(
            model_name='review2m',
            old_name='Reviews',
            new_name='Review',
        ),
        migrations.RenameField(
            model_name='review3m',
            old_name='Namess',
            new_name='Name',
        ),
        migrations.RenameField(
            model_name='review3m',
            old_name='Reviewss',
            new_name='Review',
        ),
    ]
