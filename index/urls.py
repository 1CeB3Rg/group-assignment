from django.urls import path
from . import views

urlpatterns = [
    path('',views.home,name='index-home'),
    path('table/',views.table,name='transactionTable'),
    path('item_list',views.itemlist,name='index-itemlist'),
    path('coupon/',views.coupon,name='index-coupon'),
    path('item1',views.item1,name='index-item1'),
    path('item2',views.item2,name='index-item2'),
    path('item3',views.item3,name='index-item3'),
    path('buyform/',views.buyerformview,name='buyform'),

]
