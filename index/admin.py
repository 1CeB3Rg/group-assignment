from django.contrib import admin
from .models import BuyForm, Coupon, Review1M, Review2M, Review3M

# Register your models here.

admin.site.register(BuyForm)
admin.site.register(Coupon)
admin.site.register(Review1M)
admin.site.register(Review2M)
admin.site.register(Review3M)