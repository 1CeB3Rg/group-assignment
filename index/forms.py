from django import forms
from .models import BuyForm, Coupon, Review1M, Review2M, Review3M
from django.forms import ModelForm

class BuyerForm(forms.ModelForm):
    class Meta:
        model = BuyForm
        fields = '__all__'

class CouponForm(forms.ModelForm):
    class Meta:
        model = Coupon
        fields = '__all__'
class Review1(forms.ModelForm):
    class Meta:
        model=Review1M
        fields='__all__'
class Review2(forms.ModelForm):
    class Meta:
        model=Review2M
        fields='__all__'
class Review3(forms.ModelForm):
    class Meta:
        model=Review3M
        fields='__all__'
