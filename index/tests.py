

from django.test import TestCase,Client
from index.views import *
from index.models import *
# Create your tests here.
class Index(TestCase):
    def test_url_exist(self):
            response= Client().get('/')
            self.assertEqual(response.status_code,200)
    def test_home_using_template(self):
            response = Client().get('/')
            self.assertTemplateUsed(response, 'home.html')
    def test_url_exist_2(self):
            response= Client().get('/buyform/')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_2(self):
            response = Client().get('/buyform/')
            self.assertTemplateUsed(response, 'buyerform.html')
    #def test_form_saved(self):
    #        response= self.client.post('/buyform/',data={'name':'Nyoman','items':'item1','total':'2','coupon':'None','email':"2000-01-01",'email':'lol@gmail.com','phone':'0','address':'None'})
    #        count=BuyForm.objects.all().count()
    #        self.assertEqual(count,1)
    def test_url_exist_3(self):
            response= Client().get('/coupon/')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_3(self):
            response = Client().get('/coupon/')
            self.assertTemplateUsed(response, 'coupon.html')
    def test_url_exist_4(self):
            response= Client().get('/item1')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_4(self):
            response = Client().get('/item1')
            self.assertTemplateUsed(response, 'item1.html')
    def test_url_exist_5(self):
            response= Client().get('/item2')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_5(self):
            response = Client().get('/item2')
            self.assertTemplateUsed(response, 'item2.html')
    def test_url_exist_6(self):
            response= Client().get('/item3')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_6(self):
            response = Client().get('/item3')
            self.assertTemplateUsed(response, 'item3.html')
    def test_url_exist_7(self):
            response= Client().get('/table/')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_7(self):
            response = Client().get('/table/')
            self.assertTemplateUsed(response, 'table.html')
    def test_url_exist_8(self):
            response= Client().get('/item_list')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_8(self):
            response = Client().get('/item_list')
            self.assertTemplateUsed(response, 'item-list.html')
    def test_model_exist(self):
            Review3M.objects.create(Name="Nyoman",Star="5",Review="OK")
            count=Review3M.objects.all().count()
            self.assertEqual(count,1)
    def test_model_exist_2(self):
            Review2M.objects.create(Name="Nyoman",Star="5",Review="OK")
            count=Review2M.objects.all().count()
            self.assertEqual(count,1)
    def test_model_exist_3(self):
            Review1M.objects.create(Name="Nyoman",Star="5",Review="OK")
            count=Review1M.objects.all().count()
            self.assertEqual(count,1)
    def test_model_exist_4(self):
            BuyForm.objects.create(name="Nyoman",items="a",total='2',coupon="None",date="2000-01-01",email="",phone="0",address="None")
            count=BuyForm.objects.all().count()
            self.assertEqual(count,1)
    
