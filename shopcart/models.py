from django.db import models

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=250, default='null')
    year = models.CharField(max_length=50, default='null')
    image = models.ImageField(upload_to='products/')
    price = models.FloatField()

    
    def __str__(self):
        return self.name