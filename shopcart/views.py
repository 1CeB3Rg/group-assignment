from django.shortcuts import render

# Create your views here.

from django.shortcuts import render, redirect
from shopcart.models import Product
from django.contrib.auth.decorators import login_required
from cart.cart import Cart


def catalog(request):
    if Product.objects.count()==0:
        Product.objects.create(name="Iphone X 2017 Model",year="November 2017",image="iphonex.png",price="730")
        Product.objects.create(name="Samsung Galaxy A80",year="May 2019",image="a80.png",price="549.99")
        Product.objects.create(name="Macbook Air 2019",year="July 2019",image="macbook-air.png",price="1049.99")
    x = Product.objects.all()
    context = {'products': x}
    return render(request,'catalog.html', context)

def cartDetail(request):
    return render(request, 'cartDetail.html')

@login_required(login_url="/users/login")
def cart_add(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("catalog")


@login_required(login_url="/users/login")
def item_clear(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.remove(product)
    return redirect("cart_detail")


@login_required(login_url="/users/login")
def item_increment(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.add(product=product)
    return redirect("cart_detail")


@login_required(login_url="/users/login")
def item_decrement(request, id):
    cart = Cart(request)
    product = Product.objects.get(id=id)
    cart.decrement(product=product)
    return redirect("cart_detail")


@login_required(login_url="/users/login")
def cart_clear(request):
    cart = Cart(request)
    cart.clear()
    return redirect("cart_detail")


@login_required(login_url="/users/login")
def cart_detail(request):
    return render(request, 'cartDetail.html')

