

from django.test import TestCase,Client
from shopcart.models import Product
from shopcart.views import *
from cart.cart import Cart
# Create your tests here.
class shopcart(TestCase):
    def test_url_exist(self):
            response= Client().get('/shopcart/catalog/')
            self.assertEqual(response.status_code,200)
    def test_home_using_template(self):
            response = Client().get('/shopcart/catalog/')
            countcontent=Product.objects.all().count()
            self.assertEqual(countcontent,3)
            self.assertTemplateUsed(response, 'catalog.html')
    def test_model_exist(self):
        Product.objects.create(name="Macbook Air 2019",year="July 2019",image="macbook-air.png",price="1049.99")
        countcontent=Product.objects.all().count()
        self.assertEqual(countcontent,1)
    def test_url_exist_2(self):
            response= Client().get('/shopcart/cartDetail/')
            self.assertEqual(response.status_code,200)
    def test_home_using_template_2(self):
            response = Client().get('/shopcart/cartDetail/')
            self.assertTemplateUsed(response, 'cartDetail.html')
    

        
