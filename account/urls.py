from django.urls import path, include
from account.views import SignUp
from django.views.generic.base import TemplateView

urlpatterns = [
    path('', include('django.contrib.auth.urls')),
    path('', TemplateView.as_view(template_name='home.html'), name='home'),
    path('signup/', SignUp.as_view(), name='signup'),

]