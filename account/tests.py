
from django.test import TestCase,Client
from django.urls import resolve
from django.contrib.auth.models import User
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from shopcart.views import *
# Create your tests here.
class authenticationtest(TestCase):
    def test_url_exist(self):
            response= Client().get('/account/login/')
            self.assertEqual(response.status_code,200)
    def test_form_kept_blank(self):
        form = UserCreationForm(data={'username':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['username'],
            ["This field is required."]
        )
